package info.sabtuminggu.rizqi_1202164364_si4004_pab_modul3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class TableControllerDaftarNama extends DatabaseHandler {
    public TableControllerDaftarNama(Context context) {
        super(context);
    }

    public boolean create(OrangTerkenal orangTerkenal) {

        ContentValues values = new ContentValues();

        values.put("nama", orangTerkenal.nama);
        values.put("pekerjaan", orangTerkenal.pekerjaan);
        values.put("jenis_kelamin", orangTerkenal.jenis_kelamin);

        SQLiteDatabase db = this.getWritableDatabase();

        boolean createSuccessful = db.insert("daftar_nama", null, values) > 0;
        db.close();

        return createSuccessful;
    }

    public List<OrangTerkenal> read() {

        List<OrangTerkenal> recordsList = new ArrayList<OrangTerkenal>();

        String sql = "SELECT * FROM daftar_nama ORDER BY id DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {

                int id = Integer.parseInt(cursor.getString(cursor.getColumnIndex("id")));
                String nama = cursor.getString(cursor.getColumnIndex("nama"));
                String pekerjaan = cursor.getString(cursor.getColumnIndex("pekerjaan"));
                String jenis_kelamin = cursor.getString(cursor.getColumnIndex("jenis_kelamin"));


                OrangTerkenal orangTerkenal = new OrangTerkenal();
                orangTerkenal.id = id;
                orangTerkenal.nama = nama;
                orangTerkenal.pekerjaan = pekerjaan;


                orangTerkenal.jenis_kelamin = jenis_kelamin;
                recordsList.add(orangTerkenal);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return recordsList;
    }

    public boolean delete(int id) {
        boolean deleteSuccessful = false;

        SQLiteDatabase db = this.getWritableDatabase();
        deleteSuccessful = db.delete("daftar_nama", "id ='" + id + "'", null) > 0;
        db.close();

        return deleteSuccessful;

    }


}

