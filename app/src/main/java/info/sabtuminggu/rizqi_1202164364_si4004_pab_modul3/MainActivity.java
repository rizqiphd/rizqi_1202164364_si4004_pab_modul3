package info.sabtuminggu.rizqi_1202164364_si4004_pab_modul3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RecyclerViewAdapter.ItemClickListener {

    RecyclerViewAdapter adapter;
    CoordinatorLayout coordinatorLayout;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        recyclerView = findViewById(R.id.rv_daftar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCustomDialog();
            }
        });

        readRecords();
        enableSwipeToDeleteAndUndo();

    }

    @Override
    public void onItemClick(View view, int position) {
        Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT).show();
    }

    public void openCustomDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt_nama = (EditText) dialogView.findViewById(R.id.edt_nama);
        final EditText edt_pekerjaan = (EditText) dialogView.findViewById(R.id.edt_pekerjaan);
        final Spinner spinner_jenisKelamin = (Spinner) dialogView.findViewById(R.id.spinner);


        dialogBuilder.setTitle("Tambah Data");
        dialogBuilder.setMessage("Masukkan data yang valid");
        dialogBuilder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                String nama = edt_nama.getText().toString();
                String pekerjaan = edt_pekerjaan.getText().toString();
                String jenisKelamin = spinner_jenisKelamin.getSelectedItem().toString();

                OrangTerkenal orangTerkenal = new OrangTerkenal();
                orangTerkenal.nama = nama;
                orangTerkenal.pekerjaan = pekerjaan;
                orangTerkenal.jenis_kelamin = jenisKelamin;

                boolean createSuccessful = new TableControllerDaftarNama(getApplicationContext()).create(orangTerkenal);

                if (createSuccessful) {
                    Toast.makeText(getApplicationContext(), "Data berhasil diinput.", Toast.LENGTH_SHORT).show();
                    readRecords();
                } else {
                    Toast.makeText(getApplicationContext(), "Data gagal diinput.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });

        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void readRecords() {

        List<OrangTerkenal> orangTerkenals = new TableControllerDaftarNama(this).read();

        if (orangTerkenals.size() > 0) {
            ArrayList<String> arrayListOrang = new ArrayList<>();

            for (OrangTerkenal obj : orangTerkenals) {
//                int id = obj.id;
                String data = obj.nama + "," + obj.pekerjaan + "," + obj.jenis_kelamin + "," + obj.id;
                arrayListOrang.add(data);
            }
            // data to populate the RecyclerView with
            // set up the RecyclerView

            int orientation = getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
                recyclerView.setLayoutManager(mLayoutManager);
            } else {
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
            }

            adapter = new RecyclerViewAdapter(this, arrayListOrang);
            adapter.setClickListener(this);
            recyclerView.setAdapter(adapter);


        }
//        Jika data kosong, yaudah lah ya
        else {

        }

    }

    private void enableSwipeToDeleteAndUndo() {
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(this) {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

                final int position = viewHolder.getAdapterPosition();
                final String item = adapter.getData().get(position);
                Log.d("HAHAHA", item);
                adapter.removeItem(position);

                String[] itemSplit = item.split(",");
                final String itemId = itemSplit[3];
                boolean deleteSuccessful = new TableControllerDaftarNama(getBaseContext()).delete(Integer.parseInt(itemId));
                if (deleteSuccessful) {
                    Toast.makeText(getBaseContext(), "Data Berhasil Di Hapus.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getBaseContext(), "Data Gagal Di Hapus.", Toast.LENGTH_SHORT).show();
                }


                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, "Data dihapus dari list.", Snackbar.LENGTH_LONG);
                snackbar.setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        adapter.restoreItem(item, position);
                        recyclerView.scrollToPosition(position);
                    }
                });

                snackbar.setActionTextColor(Color.YELLOW);
                snackbar.show();
            }
        };

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchhelper.attachToRecyclerView(recyclerView);
    }
}
