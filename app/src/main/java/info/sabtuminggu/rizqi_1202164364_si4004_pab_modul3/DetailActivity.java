package info.sabtuminggu.rizqi_1202164364_si4004_pab_modul3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {
    TextView tvNamaDetail, tvPekerjaanDetail;
    ImageView imgFotoDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        tvNamaDetail = findViewById(R.id.tv_nama_detail);
        tvPekerjaanDetail = findViewById(R.id.tv_pekerjaan_detail);
        imgFotoDetail = findViewById(R.id.img_foto_detail);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            tvNamaDetail.setText(extras.getString("nama"));
            tvPekerjaanDetail.setText(extras.getString("pekerjaan"));
            if (extras.getString("jenisKelamin").equals("Male")) {
                imgFotoDetail.setImageResource(R.drawable.male);
            } else {
                imgFotoDetail.setImageResource(R.drawable.female);
            }

        }


    }
}

