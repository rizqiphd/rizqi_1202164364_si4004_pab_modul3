package info.sabtuminggu.rizqi_1202164364_si4004_pab_modul3;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<String> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    Context context;

    // data is passed into the constructor
    RecyclerViewAdapter(Context context, List<String> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_row, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String data = mData.get(position);
        String[] dataSplit = data.split(",");
        final String nama = dataSplit[0];
        final String pekerjaan = dataSplit[1];
        final String jenisKelamin = dataSplit[2];
        final String idObj = dataSplit[3];

        holder.myTextView.setVisibility(View.GONE);
        holder.tvNama.setText(nama);
        holder.tvPekerjaan.setText(pekerjaan);
        if (jenisKelamin.equals("Male")) {
            holder.imgFoto.setImageResource(R.drawable.male);
        } else {
            holder.imgFoto.setImageResource(R.drawable.female);
        }

        holder.llItemRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetailActivity.class);
                i.putExtra("nama", nama);
                i.putExtra("jenisKelamin", jenisKelamin);
                i.putExtra("pekerjaan", pekerjaan);
                context.startActivity(i);
            }
        });

        holder.llItemRow.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                boolean deleteSuccessful = new TableControllerDaftarNama(context).delete(Integer.parseInt(idObj));

                if (deleteSuccessful) {
                    Toast.makeText(context, "Data Berhasil Di Hapus.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Data Gagal Di Hapus.", Toast.LENGTH_SHORT).show();
                }

                ((MainActivity) context).readRecords();
                return deleteSuccessful;
            }
        });


    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView, tvNama, tvPekerjaan, tvJenisKelamin;
        ImageView imgFoto;
        LinearLayout llItemRow;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.tvAnimalName);
            tvNama = itemView.findViewById(R.id.tv_nama);
            tvPekerjaan = itemView.findViewById(R.id.tv_pekerjaan);
            imgFoto = itemView.findViewById(R.id.img_foto);
            llItemRow = itemView.findViewById(R.id.ll_item_row);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }


    }

    // convenience method for getting data at click position
    String getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;

    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public void removeItem(int position) {
        mData.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(String item, int position) {
        mData.add(position, item);
        notifyItemInserted(position);
    }

    public List<String> getData() {
        return mData;
    }


}


