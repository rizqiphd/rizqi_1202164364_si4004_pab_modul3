package info.sabtuminggu.rizqi_1202164364_si4004_pab_modul3;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    protected static final String DATABASE_NAME = "db_orangterkenal";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE daftar_nama " +
                "( id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "nama TEXT, " +
                "pekerjaan TEXT, " +
                "jenis_kelamin TEXT )";

        db.execSQL(sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS daftar_nama";
        db.execSQL(sql);
        onCreate(db);
    }

    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from daftar_nama");
        db.close();
    }

}

